define NEWLINE


endef

TARGET = run

INCLUDE_DIRS = $(addprefix -I, /usr/local/include/opencv4 /usr/include/pcl-1.8 /usr/include/eigen3/ /usr/include/vtk-6.3/)
OBJECTS = $(addsuffix .o, $(basename $(shell find -name '*.cpp')))

CXX = g++
CXXFLAGS += -std=c++11 -D$(shell echo $(USER) | tr a-z A-Z) -O3 -DLINUX -Wall $(INCLUDE_DIRS)
# LFLAGS += -lpthread -ljpeg -lrt -lncurses -lyaml-cpp

OPENCV_LIBRARY = `pkg-config --libs opencv4` -lboost_system -L/usr/lib/x86_64-linux-gnu -lpcl_visualization -lpcl_common -lvtkRenderingCore-6.3 -lvtkCommonDataModel-6.3 -lvtkCommonMath-6.3 -lvtkCommonCore-6.3

all: $(TARGET)

clean:
	rm -f *.a *.o $(OBJECTS) $(TARGET) core *~ *.so *.lo

%.a:
	make -C $(addprefix $(LIBRARY_ROOT), $(basename $@))

$(TARGET): $(addsuffix .a, $(LIBRARY)) $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(OBJECTS) -o $(TARGET) $(OPENCV_LIBRARY)

