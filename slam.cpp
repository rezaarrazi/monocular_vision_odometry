#include "slam.h"

using namespace cv;
using namespace std;

namespace slam {

    Frame::Frame() :
        frame_id(0),
        R(cv::Mat::eye(3, 3, CV_64FC1)),
        t(cv::Mat::zeros(3, 1, CV_64FC1)),
        camera_pose(cv::Mat::eye(3, 4, CV_64FC1))
    {
    }

    Frame::Frame(cv::Mat image_, int frame_id_) :
        frame_id(frame_id_),
        previous_frame_id(frame_id_-1),
        image(image_),
        R(cv::Mat::eye(3, 3, CV_64FC1)),
        t(cv::Mat::zeros(3, 1, CV_64FC1)),
        camera_pose(cv::Mat::eye(3, 4, CV_64FC1))
    {
        cv::cvtColor(image, image_gray, COLOR_BGR2GRAY);
    }

    void FeatureTrack::featureExtractor(Mat img, vector<Point2f>& points, int feature) {
        Mat descriptors;
        vector<KeyPoint> keypoints;

        if(feature == FEATURE_FAST){
            // FAST algorithm
            int fast_threshold = 20;
            bool nonmaxSuppression = true;
            FAST(img, keypoints, fast_threshold, nonmaxSuppression);
            KeyPoint::convert(keypoints, points, vector<int>());
        }
        else if(feature == FEATURE_ORB){
            // ORB algorithm
            Ptr<Feature2D> orb = ORB::create(MAX_NUM_FEAT);
            orb->detectAndCompute(img, Mat(), keypoints, descriptors);
            KeyPoint::convert(keypoints, points, vector<int>());
        }
        else if(feature == FEATURE_SHI_TOMASI){
            // Shi-Tomasi algorithm
            goodFeaturesToTrack(img, points, MAX_NUM_FEAT, 0.01, 10);
        }
    }

    void FeatureTrack::featureTracker(Mat img_1, Mat img_2, vector<Point2f>& points1, vector<Point2f>& points2, vector<uchar>& status)	{ 
        vector<float> err;					
        Size winSize=Size(21,21);																								
        TermCriteria termcrit=TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.01);

        calcOpticalFlowPyrLK(img_1, img_2, points1, points2, status, err, winSize, 3, termcrit, 0, 0.001);

        //getting rid of points for which the KLT tracking failed or those who have gone outside the frame
        int indexCorrection = 0;
        for( unsigned int i=0; i<status.size(); i++){
            Point2d pt = points2.at(i - indexCorrection);
            if ((status.at(i) == 0)||(pt.x<0)||(pt.y<0))	{
                if((pt.x<0)||(pt.y<0)) {
                    status.at(i) = 0;
                }
                points1.erase (points1.begin() + (i - indexCorrection));
                points2.erase (points2.begin() + (i - indexCorrection));
                indexCorrection++;
            }
        }
    }

    void FeatureTrack::goodMatchesFeatures(vector<Point2f>& points1, vector<Point2f>& points2, Mat mask) {
        vector<Point2f> inlier_match_points1, inlier_match_points2;
        for(int i = 0; i < mask.rows; i++) {
            if(mask.at<unsigned char>(i)){
                inlier_match_points1.push_back 
                            (Point2d( points1[i].x, points1[i].y));
                inlier_match_points2.push_back 
                            (Point2d( points2[i].x, points2[i].y));
            }
        }
        points1 = inlier_match_points1;
        points2 = inlier_match_points2;
    }

    SharedData::SharedData(Mat K_) :
        base_R(cv::Mat::eye(3, 3, CV_64FC1)),
        base_t(cv::Mat::zeros(3, 1, CV_64FC1))
    {
        K_.convertTo(K, CV_64F);
        focal = K_.at<double>(0,0);
        pp = Point2d(K_.at<double>(0,2), K_.at<double>(1,2));
    }

    Eigen::Affine3f SharedData::Mat2Afiine3f(Mat camera_pose){
        active_camera_pose = Eigen::Affine3f::Identity();
        
        //   M11 M21 M31 T1
        //   M12 M22 M32 T2
        //   M13 M23 M33 T3

        // rotation	
        active_camera_pose(0, 0) = camera_pose.at<double>(0, 0);
        active_camera_pose(0, 1) = camera_pose.at<double>(0, 1);
        active_camera_pose(0, 2) = camera_pose.at<double>(0, 2);
        active_camera_pose(1, 0) = camera_pose.at<double>(1, 0);
        active_camera_pose(1, 1) = camera_pose.at<double>(1, 1);
        active_camera_pose(1, 2) = camera_pose.at<double>(1, 2);
        active_camera_pose(2, 0) = camera_pose.at<double>(2, 0);
        active_camera_pose(2, 1) = camera_pose.at<double>(2, 1);
        active_camera_pose(2, 2) = camera_pose.at<double>(2, 2);

        // translation
        active_camera_pose(0, 3) = camera_pose.at<double>(0, 3);
        active_camera_pose(1, 3) = camera_pose.at<double>(1, 3);
        active_camera_pose(2, 3) = camera_pose.at<double>(2, 3);
        
        return active_camera_pose;
    }
}