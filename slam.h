#include "opencv2/opencv.hpp"
#include <pcl/common/common_headers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>

#include <iostream>

using namespace cv;
using namespace std;

#define MAX_NUM_FEAT 20000
#define MIN_NUM_FEAT 2000

#define FEATURE_FAST 0
#define FEATURE_ORB 1
#define FEATURE_SHI_TOMASI 2

namespace slam {
    
    class Frame {
	public:
        int frame_id;
        int previous_frame_id;
        cv::Mat image;
        cv::Mat image_gray;
        vector<KeyPoint> keypoints;
        vector<Point2f> feature_points;
        
        cv::Mat R;
        cv::Mat t;
        cv::Mat camera_pose;

        Frame();
        Frame(cv::Mat _image, int frame_id);
	};

    class FeatureTrack {
    public:
        static void featureExtractor(Mat img, vector<Point2f>& points, int feature);
        static void featureTracker(Mat img_1, Mat img_2, vector<Point2f>& points1, vector<Point2f>& points2, vector<uchar>& status);
        static void goodMatchesFeatures(vector<Point2f>& points1, vector<Point2f>& points2, Mat mask);
    };

    class SharedData {
    public:
        cv::Mat K;
        double focal;
        cv::Point2d pp;

        cv::Mat base_R;
		cv::Mat base_t;

        Eigen::Affine3f active_camera_pose;

        SharedData();
        SharedData(Mat K_);

        Eigen::Affine3f Mat2Afiine3f(Mat camera_pose);
    };
}