#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include <iostream>
#include <ctype.h>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <vector>
#include <ctime>
#include <sstream>
#include <fstream>
#include <string>

#include "slam.h"

using namespace cv;
using namespace std;


#define START_FRAME 50
#define MAX_FRAME 500//4540

#define DISPLAY3D true
#define CLOUD_NAME "Triangulated Point Cloud"

// Checks if a matrix is a valid rotation matrix.
bool isRotationMatrix(Mat &R)
{
    Mat Rt;
    transpose(R, Rt);
    Mat shouldBeIdentity = Rt * R;
    Mat I = Mat::eye(3,3, shouldBeIdentity.type());
    
    return  norm(I, shouldBeIdentity) < 1e-6;
    
}

double getAbsoluteScale(int frame_id, int sequence_id, double z_cal, double& x_truth, double& y_truth, double& z_truth)	{
  
    string line;
    int i = 0;
    ifstream myfile ("/mnt/d/Reza/Dokumen/datasets/kitti/data_odometry/poses/poses/00.txt");
    double x =0, y=0, z = 0;
    double x_prev=0, y_prev=0, z_prev=0;
    if (myfile.is_open())
    {
        while (( getline (myfile,line) ) && (i<=frame_id))
        {
            z_prev = z;
            x_prev = x;
            y_prev = y;
            std::istringstream in(line);
            //cout << line << '\n';
            for (int j=0; j<12; j++)  {
                in >> z ;
                if (j==7) y=z;
                if (j==3)  x=z;
            }
            
            if (i==frame_id){
                x_truth = x;
                y_truth = y;
                z_truth = z;
            }

            i++;
        }
        myfile.close();
    }
    else {
        cout << "Unable to open file";
        return 0;
    }

    return sqrt((x-x_prev)*(x-x_prev) + (y-y_prev)*(y-y_prev) + (z-z_prev)*(z-z_prev)) ;
}

int main( int argc, char** argv ) {

    string dataset_directory = argv[1];

    bool initial = true;
    int initial_x = 0;
    int initial_y = 0;
    int initial_z = 0;

    double scale = 1.00;
    char filename1[200];
    char filename2[200];
    sprintf(filename1, "%s/%06d.png", dataset_directory.c_str(), 0);
    sprintf(filename2, "%s/%06d.png", dataset_directory.c_str(), 1);

    int fontFace = FONT_HERSHEY_PLAIN;
    double fontScale = 1;
    int thickness = 1;

    // Camera intristic parameter matrix
    cv::Mat K = (cv::Mat_<double>(3,3) <<   7.188560000000e+02, 0.000000000000e+00, 6.071928000000e+02,
                                            0.000000000000e+00, 7.188560000000e+02, 1.852157000000e+02,
                                            0.000000000000e+00, 0.000000000000e+00, 1.000000000000e+00);
    
    cout << "K:" << endl;
    cout << K << endl;
    
    slam::Frame current_frame;
    slam::Frame previous_frame;

    slam::SharedData shared_data(K);

    // cout << focal << endl;
    // cout << pp << endl;

    char filename[100];

    namedWindow( "Road facing camera", WINDOW_AUTOSIZE );
    namedWindow( "Trajectory", WINDOW_AUTOSIZE );// Create a window for display.

    Mat traj = Mat::zeros(600, 600, CV_8UC3);
    
    vector<Eigen::Affine3f> camera_poses;
    
    for(int numFrame=START_FRAME; numFrame < MAX_FRAME; numFrame++) {
        sprintf(filename, "%s/%06d.png", dataset_directory.c_str(), numFrame);
        
        current_frame = slam::Frame(imread(filename, IMREAD_COLOR), numFrame);
        
        if ( !current_frame.image.data) { 
            std::cout<< " --(!) Error reading images " << std::endl; return -1;
        }

        if (previous_frame.image.empty()){
            // initial
            previous_frame = current_frame;
            continue;
        }

        if (previous_frame.feature_points.size() < MIN_NUM_FEAT)	{
            cout << "Number of tracked features reduced to " << previous_frame.feature_points.size() << endl;
            cout << "trigerring redetection" << endl;
            slam::FeatureTrack::featureExtractor(previous_frame.image_gray, previous_frame.feature_points, FEATURE_FAST);
            // prevFeatures = currFeatures;
        }

        vector<uchar> status;
        slam::FeatureTrack::featureTracker(previous_frame.image_gray, current_frame.image_gray, previous_frame.feature_points, current_frame.feature_points, status);
        
        Mat E, mask;
        E = findEssentialMat(current_frame.feature_points, previous_frame.feature_points, shared_data.focal, shared_data.pp, RANSAC, 0.999, 1.0, mask);
        recoverPose(E, current_frame.feature_points, previous_frame.feature_points, current_frame.R, current_frame.t, shared_data.focal, shared_data.pp, mask);
        
        if (numFrame < 2){
            previous_frame = current_frame;
            shared_data.base_R = current_frame.R;
            shared_data.base_t = current_frame.t;

            continue;
        }
        cout << "____________________" << endl;
        printf("\n*** frame %d ***\n", numFrame);

        slam::FeatureTrack::goodMatchesFeatures(current_frame.feature_points, previous_frame.feature_points, mask);

        double x_truth, y_truth, z_truth;
        scale = getAbsoluteScale(numFrame, 0, current_frame.t.at<double>(2), x_truth, y_truth, z_truth);

        if (initial){
            initial = false;
            initial_x = x_truth;
            initial_y = y_truth;
            initial_z = z_truth;
        }

        x_truth -= initial_x;
        y_truth -= initial_y;
        z_truth -= initial_z;
        cout << "Scale is " << scale << endl;
        
        if ((scale>0.1)&&(current_frame.t.at<double>(2) > current_frame.t.at<double>(0)) && (current_frame.t.at<double>(2) > current_frame.t.at<double>(1))) {
            shared_data.base_t = shared_data.base_t + scale*(shared_data.base_R * current_frame.t);
            shared_data.base_R = current_frame.R * shared_data.base_R;

            shared_data.base_R.copyTo(current_frame.camera_pose.rowRange(0,3).colRange(0,3));
            shared_data.base_t.copyTo(current_frame.camera_pose.rowRange(0,3).col(3));
        }
        else {
            //cout << "scale below 0.1, or incorrect translation" << endl;
        }

        for(unsigned int i=0; i<current_frame.feature_points.size(); i++){
            circle(current_frame.image, current_frame.feature_points[i], 1, Scalar(0, 0, 255), -1);
            circle(current_frame.image, previous_frame.feature_points[i], 1, Scalar(255, 0, 0), -1);
            line(current_frame.image, current_frame.feature_points[i], previous_frame.feature_points[i], Scalar(255, 0, 255));
        }

        int x = int(shared_data.base_t.at<double>(0)) + 300;
        int y = int(shared_data.base_t.at<double>(2)) + 100;
        circle(traj, Point(x, y) ,1, CV_RGB(255,0,0), 2);
        circle(traj, Point(int(x_truth+300), int(z_truth+100)) ,1, CV_RGB(0,0,255), 2);
        Mat traj_mirror = Mat::zeros(600, 600, CV_8UC3);
        flip(traj, traj_mirror, +1);
        // Point3d rotation_euler = rotationMatrixToEulerAngles(shared_data.base_R);
        // cout << "angle;" << endl;
        // cout << rotation_euler.x << ", " << rotation_euler.y << ", " << rotation_euler.z << endl;

        char text_truth[100];
        char text[100];
    
        rectangle( traj_mirror, Point(10, 30), Point(550, 65), CV_RGB(0,0,0), -1);
        sprintf(text_truth, "Ground truth: x = %02fm y = %02fm z = %02fm", x_truth, y_truth, z_truth);
        sprintf(text, "Odometry: x = %02fm y = %02fm z = %02fm", shared_data.base_t.at<double>(0), shared_data.base_t.at<double>(1), shared_data.base_t.at<double>(2));
        
        putText(traj_mirror, text_truth, Point(10, 50), fontFace, fontScale, Scalar(255, 0, 0), thickness, 8);
        putText(traj_mirror, text, Point(10, 65), fontFace, fontScale, Scalar(0, 0, 255), thickness, 8);

        imshow( "Road facing camera", current_frame.image );
        imshow( "Trajectory", traj_mirror );
        
        if (waitKey(1) == 'n')
            break;

        previous_frame = current_frame;
    }

    return 0;
}