# Monocular SLAM using OpenCV and PCL

----
## SLAM
see [Wikipedia](https://en.wikipedia.org/wiki/Markdown)

> In computational geometry, simultaneous localization and mapping (SLAM) is the computational problem of constructing or updating a map of an unknown environment while simultaneously keeping track of an agent's location within it.

----
## approach
In each frame we extract features and then track the features on the next frames. If matches are found along the epipolar line, we triangulate the points and determine their 3D position.

Based on the 3D-3D correspondences, we estimate the transformation between the frames by SVD decomposition with RANSAC.

----
## dependencies
1. OpenCV.

## usage
    $ make
    $ ./run <directory_to_dataset>

----
